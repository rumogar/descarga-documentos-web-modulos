from cache import Cache

if __name__ == '__main__':
    print("Test Cache class")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/') #guardamos diferentes url
    c.retrieve('https://www.aulavirtual.urjc.es')
    c.show('https://www.aulavirtual.urjc.es') #muestra al ejecutar el código html 
    c.show_all() #muestra la self.cache
