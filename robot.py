import urllib.request

class Robot:

    def __init__(self, url):
        self.url = url
        self.retrieved = False #comprobamos si se guarda la url

    def retrieve(self): #función para descargar el archivo
        if not self.retrieved:
            print("Descargando...")
            f = urllib.request.urlopen(self.url)
            self.content = f.read().decode('utf-8')
            self.retrieved = True

    def content(self):  #lee archivo
        self.retrieve()
        return self.content

    def show(self): #imprime archivo
        print(self.content())


