from robot import Robot

class Cache: #creamos la clase

    def __init__(self):
        self.cache = {} #aqui guardamos las urls

    def retrieve(self, url):
        if url not in self.cache: 
            bot = Robot(url = url)
            self.cache[url] = bot

    def content(self, url): #mostramos el contenido de la url
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):  #escribe la ultima url
        print(self.content(url))
 
    def show_all(self):  #escribe las url almacenadas en self.cache
        for url in self.cache:
            print(url)


